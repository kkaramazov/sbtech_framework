package com.sbtech.sbtechplatformutilities.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbtech.sbtechplatformutilities.authservice.AuthenticationService;
import com.sbtech.sbtechplatformutilities.geoverifierservice.GeoVerifierService;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInitializer {
    private static RetrofitInitializer sInstance;
    private Retrofit retrofit;
    private static ChangeHostInterceptor changeHostInterceptor = new ChangeHostInterceptor();

    private RetrofitInitializer() {

    }

    public static RetrofitInitializer initialize(String baseUrl) {
        if(sInstance == null) {
            Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().setLenient().create();
            sInstance = new RetrofitInitializer();
            sInstance.retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .client(getHttpClient())
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return sInstance;
    }

    private static OkHttpClient getHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message ->Log.d(RetrofitInitializer.class.getSimpleName(), message));
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        HttpLoggingInterceptor loggingHeaders = new HttpLoggingInterceptor(message ->Log.d(RetrofitInitializer.class.getSimpleName(), message));
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        HttpLoggingInterceptor loggingBody = new HttpLoggingInterceptor(message ->Log.d(RetrofitInitializer.class.getSimpleName(), message));
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(loggingHeaders)
                .addInterceptor(logging)
                .addInterceptor(loggingBody)
                .addInterceptor(changeHostInterceptor)
                .build();
    }

    public static void setHost(String host) {
        changeHostInterceptor.setHost(host);
    }

    public AuthenticationService getAuthenticationService() {
        return retrofit.create(AuthenticationService.class);
    }

    public GeoVerifierService getGeoVerifierService() {
        return retrofit.create(GeoVerifierService.class);
    }
}
