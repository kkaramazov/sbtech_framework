package com.sbtech.sbtechplatformutilities.retrofit;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ChangeHostInterceptor implements Interceptor {

    private String host;

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String host = this.host;
        if(host != null) {
            List<String> pathSegments = request.url().encodedPathSegments();
            StringBuilder newUrlString = new StringBuilder(host);
            for(String pathSegment : pathSegments) {
                newUrlString.append(pathSegment).append("/");
            }
            HttpUrl newUrl = HttpUrl.parse(newUrlString.toString());
            request = request.newBuilder()
                    .url(newUrl)
                    .build();
        }
        return chain.proceed(request);
    }
}
