package com.sbtech.sbtechplatformutilities.authservice;

import com.sbtech.sbtechplatformutilities.authservice.model.AuthenticationResponse;

import io.reactivex.Single;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AuthenticationService {
    @POST("/{operatorName}/auth/v1/anonymous")
    Single<AuthenticationResponse> authenticatePlatform(@Header("operatorId") String operatorId, @Path("operatorName") String operatorName);

    @POST("/{operatorName}/auth/v1/anonymous")
    Single<AuthenticationResponse> authenticateSeamless(@Header("domainId") String domainId, @Path("operatorName") String operatorName);
}
