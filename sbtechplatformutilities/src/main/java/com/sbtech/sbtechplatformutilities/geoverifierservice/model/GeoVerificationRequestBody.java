package com.sbtech.sbtechplatformutilities.geoverifierservice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sbtech.sbtechplatformutilities.base.BaseResponseObject;

public class GeoVerificationRequestBody extends BaseResponseObject{

    @SerializedName("GeoToken")
    @Expose
    private String geoToken;

    public GeoVerificationRequestBody(String geoToken) {
        this.geoToken = geoToken;
    }

    public String getGeoToken() {
        return geoToken;
    }

    public void setGeoToken(String geoToken) {
        this.geoToken = geoToken;
    }

}
