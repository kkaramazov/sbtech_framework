package com.sbtech.sbtechplatformutilities.geoverifierservice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sbtech.sbtechplatformutilities.base.BaseResponseObject;

public class CustomerLocationResponse extends BaseResponseObject{

    @SerializedName("nbf")
    @Expose
    private Integer nbf;
    @SerializedName("exp")
    @Expose
    private Integer exp;
    @SerializedName("SiteID")
    @Expose
    private Integer siteID;
    @SerializedName("CustomerID")
    @Expose
    private Integer customerID;
    @SerializedName("CustomerStatus")
    @Expose
    private String customerStatus;
    @SerializedName("ExpDate")
    @Expose
    private String expDate;
    @SerializedName("ErrorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;

    public Integer getNbf() {
        return nbf;
    }

    public void setNbf(Integer nbf) {
        this.nbf = nbf;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getSiteID() {
        return siteID;
    }

    public void setSiteID(Integer siteID) {
        this.siteID = siteID;
    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
