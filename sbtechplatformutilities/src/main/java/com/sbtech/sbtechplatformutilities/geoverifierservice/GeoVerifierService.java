package com.sbtech.sbtechplatformutilities.geoverifierservice;

import com.sbtech.sbtechplatformutilities.geoverifierservice.model.CustomerLocationResponse;
import com.sbtech.sbtechplatformutilities.geoverifierservice.model.GeoLicenceResponse;
import com.sbtech.sbtechplatformutilities.geoverifierservice.model.GeoVerificationRequestBody;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GeoVerifierService {

    @GET("/GeoVerifier/GetLicense")
    public Single<GeoLicenceResponse> getLicence(@Header("Authorization") String authToken);

    @POST("/GeoVerifier/CheckCustomerLocation")
    public Single<CustomerLocationResponse> checkCustomerLocation(@Header("Authorization") String jwtToken, @Body GeoVerificationRequestBody body);

}
