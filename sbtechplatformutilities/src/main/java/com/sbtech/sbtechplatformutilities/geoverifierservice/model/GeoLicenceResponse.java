package com.sbtech.sbtechplatformutilities.geoverifierservice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sbtech.sbtechplatformutilities.base.BaseResponseObject;

public class GeoLicenceResponse extends BaseResponseObject{

    @SerializedName("SiteID")
    @Expose
    private Integer siteID;
    @SerializedName("License")
    @Expose
    private String license;
    @SerializedName("LicenseExpirationDate")
    @Expose
    private String licenseExpirationDate;
    @SerializedName("ErrorCode")
    @Expose
    private Integer errorCode;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;

    public Integer getSiteID() {
        return siteID;
    }

    public void setSiteID(Integer siteID) {
        this.siteID = siteID;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    public void setLicenseExpirationDate(String licenseExpirationDate) {
        this.licenseExpirationDate = licenseExpirationDate;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
