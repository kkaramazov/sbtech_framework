package com.sbtech.sbtechplatformutilities.geoverifierservice.ui;

import android.arch.lifecycle.LifecycleObserver;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {

    private T binding;
    /**
     *Here we initialize the data binding
     * and initialize the UI as whole.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        initUI();
        return binding.getRoot();
    }

    protected final void addLifecycleObserver(LifecycleObserver observer) {
        getLifecycle().addObserver(observer);
    }

    public T getBinding() {
        return binding;
    }

    /**
     *
     * @return the layout resource id. ex.: R.layout.some_layout
     */
    protected abstract int getLayoutId();

    /**
     * Do all initial UI stuff here
     */
    protected abstract void initUI();

    /**
     *
     * @param clz Class of the fragment
     * @return Simple name of the class. We use this for a backstack tag
     */
    public static final String getBackstackTag(Class<? extends BaseFragment> clz){
        return clz.getSimpleName();
    }
}

