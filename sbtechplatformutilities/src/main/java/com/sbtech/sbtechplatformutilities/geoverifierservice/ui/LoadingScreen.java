package com.sbtech.sbtechplatformutilities.geoverifierservice.ui;

import android.os.Bundle;

import com.sbtech.sbtechplatformutilities.R;
import com.sbtech.sbtechplatformutilities.databinding.FragmentLoadingBinding;

public class LoadingScreen extends BaseFragment<FragmentLoadingBinding> {

    public static LoadingScreen getInstance(Bundle args) {
        LoadingScreen fragment = new LoadingScreen();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_loading;
    }

    @Override
    protected void initUI() {
    }
}
