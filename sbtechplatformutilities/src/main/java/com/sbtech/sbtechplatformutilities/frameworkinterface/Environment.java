package com.sbtech.sbtechplatformutilities.frameworkinterface;

public enum Environment {

    STG("https://stgapi.sbtech.com/", "http://geoverifierapi.staging.sbtech.com/"), PROD("https://sbapi.sbtech.com/", "http://geoverifierapi.staging.sbtech.com/");

    private String authEndpoint;
    private String geoVerifierEndpoint;
    Environment(String authEndpoint, String geoVerifierEndpoint) {
        this.authEndpoint = authEndpoint;
        this.geoVerifierEndpoint = geoVerifierEndpoint;
    }

    public String getAuthEndpoint() {
        return authEndpoint;
    }

    public String getGeoVerifierEndpoint() {
        return geoVerifierEndpoint;
    }
}
