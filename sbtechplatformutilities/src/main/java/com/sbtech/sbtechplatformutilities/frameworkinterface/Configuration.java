package com.sbtech.sbtechplatformutilities.frameworkinterface;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Configuration {

    private static Configuration sInstance;

    private Configuration() {}

    private String token;

    public String operatorName;

    public Environment environment;

    public String locale;

    public String id;

    public boolean seamless;

    public static Configuration init(@NonNull String operatorName, @NonNull Environment environment, @Nullable String locale, @NonNull String id, @NonNull boolean seamless) {
        Configuration configuration = new Configuration();
        configuration.operatorName = operatorName;
        configuration.environment = environment;
        configuration.locale = locale;
        configuration.id = id;
        configuration.seamless = seamless;
        return configuration;
    }
}
