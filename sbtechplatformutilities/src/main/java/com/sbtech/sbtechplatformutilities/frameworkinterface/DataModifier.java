package com.sbtech.sbtechplatformutilities.frameworkinterface;

import com.sbtech.sbtechplatformutilities.base.BaseResponseObject;

import java.util.List;

public interface DataModifier<T extends BaseResponseObject> {
    void modify(T data);
    void modifyAll(List<T> data);
}
