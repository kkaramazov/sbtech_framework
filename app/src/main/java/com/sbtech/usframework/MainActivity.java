package com.sbtech.usframework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.sbtech.sbtechplatformutilities.frameworkinterface.API;
import com.sbtech.sbtechplatformutilities.frameworkinterface.Configuration;
import com.sbtech.sbtechplatformutilities.frameworkinterface.Environment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        API.getInstance().configure(Configuration.init("whltemplate", Environment.PROD, "en", "ERcwQOOOcuxUBfyVFqth", false));
        API.getInstance().authorize()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(authenticationResponse -> {
                    API.getInstance().getGeoComplyLicence(authenticationResponse.getJwt())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .doOnSuccess(geoLicenceResponse -> {
                                Log.d("SUCCESS", geoLicenceResponse.toString());
                            })
                            .subscribe();
                })
                .doOnError(throwable -> {
                    Log.d("MainActivity", "error");
                })
                .subscribe();

    }
}
